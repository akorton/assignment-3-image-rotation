#include "../include/utils.h"
#include "../include/bmp.h"
#include "../include/transform.h"


int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Valid number of arguments is 3.\n");
        return 0;
    }
    FILE* file_in_ptr;
    enum open_result open_result_in = open_file(argv[1], "r", &file_in_ptr);
    if (open_result_in == OPEN_WRONG){
        printf("Can't open %s file.\n", argv[1]);
        return 0;
    }
    printf("File %s successfully opened!\n", argv[1]);

    struct image img;
    enum read_status read_res = from_bmp(file_in_ptr, &img);
    enum check_status status = print_read_result(read_res);
    if (status == RETURN){
        close_file(file_in_ptr);
        return 0;
    }
    enum close_result close_result_in = close_file(file_in_ptr);
    if (close_result_in == CLOSE_WRONG){
        printf("Can't close %s file.\n", argv[1]);
        free_image(&img);
        return 0;
    }
    printf("File %s successfully closed!\n", argv[1]);

    struct image rotated_img;
    enum rotate_result rotate_res = rotate(img, &rotated_img);
    if (rotate_res == ROTATE_CREATE_IMAGE_ERROR){
        free_image(&img);
        printf("Can not create image during rotate.\n");
        return 0;
    }
    printf("Successfully transformed!\n");
    free_image(&img);

    FILE* file_out_ptr;
    enum open_result open_result_out = open_file(argv[2], "wb", &file_out_ptr);
    if (open_result_out == OPEN_WRONG){
        printf("Can't open %s file.\n", argv[2]);
        return 0;
    }
    printf("File %s successfully opened!\n", argv[2]);

    enum write_status write_result = to_bmp(file_out_ptr, &rotated_img);
    free_image(&rotated_img);
    if (write_result == WRITE_HEADER_ERROR){
        printf("Can not write header.\n");
        close_file(file_out_ptr);
        return 0;
    }
    if (write_result == WRITE_DATA_ERROR){
        printf("Can not write data.\n");
        close_file(file_out_ptr);
        return  0;
    }
    if (write_result == WRITE_MALLOC_DATA_ERROR){
        printf("Can not malloc enough memory for data.\n");
        close_file(file_out_ptr);
        return 0;
    }
    printf("Writed successfully!\n");
    enum close_result close_result_out = close_file(file_out_ptr);
    if (close_result_out == CLOSE_WRONG){
        printf("Can't close %s file.\n", argv[2]);
        return 0;
    }
    printf("File %s successfully closed!\n", argv[2]);


    return 0;
}
