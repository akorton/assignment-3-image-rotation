#include "../include/utils.h"

enum open_result open_file(const char* filename, const char* mode, FILE** file_ptr_ptr){
    *file_ptr_ptr = fopen(filename, mode);
    if (!*file_ptr_ptr) return OPEN_WRONG;
    return OPEN_OK;
}


enum close_result close_file(FILE* fileptr){
    int res = fclose(fileptr);
    if (res == EOF) return CLOSE_WRONG;
    return CLOSE_OK;
}
