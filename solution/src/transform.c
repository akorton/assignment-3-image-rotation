#include "../include/transform.h"

uint64_t get_result_pixel(uint64_t width, size_t i, size_t j){
    return i*width+width - j - 1;
}

uint64_t get_source_pixel(uint64_t width, size_t i, size_t j){
    return j*width + i;
}

enum rotate_result rotate(struct image const source, struct image* rotated){
    enum create_result create_res = create_image(source.height, source.width, rotated);
    if (create_res == CREATE_WRONG) return ROTATE_CREATE_IMAGE_ERROR;
    for (size_t i = 0; i < source.width;i++){
        for (size_t j = 0; j < source.height; j++){
            rotated->data[get_result_pixel(rotated->width, i, j)] = source.data[get_source_pixel(source.width, i, j)];
        }
    }
    return ROTATE_OK;
}
