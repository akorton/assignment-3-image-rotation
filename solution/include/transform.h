#pragma once
#include "image.h"

enum rotate_result{
    ROTATE_OK,
    ROTATE_CREATE_IMAGE_ERROR
};

enum rotate_result rotate(struct image const source, struct image* rotated);
